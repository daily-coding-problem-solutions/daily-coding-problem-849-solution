fn main() {
    let l = longest();
    let result = collatz(l);
    println!("longest {}", l);
    println!("{:?}", result);
    println!("steps {}", result.len());
}

fn longest() -> u64 {
    let mut max: u64 = 0;
    for i in 1..1000000 {
        let rt = collatz_len(i);
        if rt > max {
            max = i;
        }
    }

    max
}

fn collatz(n: u64) -> Vec<u64> {
    let mut next = n;
    let mut seq = vec![];
    if next == 0 {
        return seq;
    }
    loop {
        seq.push(next);

        next = match next % 2 == 0 {
            true => next / 2,
            false => 3 * next + 1,
        };
        if next == 1 {
            seq.push(1);
            return seq;
        }
    }
}

fn collatz_len(n: u64) -> u64 {
    let mut next = n;
    let mut count = 1;
    loop {
        next = match next % 2 == 0 {
            true => next / 2,
            false => 3 * next + 1,
        };
        count += 1;
        if next == 1 {
            return count;
        }

    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_12_is_10() {
        assert_eq!(collatz_len(12), 10);
    }
}
